// ---------------------- Clock ---------------------------------
setInterval(()=>{
    var now = new Date()
    var date = now.toDateString()
    var time = now.toLocaleTimeString();
    document.getElementById("time-now").innerHTML = `${date} ${time}`
},1000)
// ---------------------------------------------------------------

let startDay 
let stopDay 
let totalTime
let totalMin
let pay

// ------------------- Function Start ------------------------------------
document.getElementById("btn-start").onclick = ()=>{

    document.getElementById("btn-stop").style.display = 'block'
    document.getElementById("btn-clear").style.display = 'none'
    document.getElementById("btn-start").style.display = 'none'

    startDay = new Date()
    document.getElementById("time-start").innerHTML =  startDay.toLocaleString('en-US', {hour: 'numeric', minute: 'numeric',hour12:true});
}

// --------------------Function Stop ------------------------------------
document.getElementById("btn-stop").onclick = ()=>{

    document.getElementById("btn-clear").style.display = 'block'
    document.getElementById("btn-start").style.display = 'none'
    document.getElementById("btn-stop").style.display = 'none'
  
    stopDay = new Date()
//----------------------Calculate---------------------------------- 
    totalTime = stopDay - startDay
    totalMin = Math.round(totalTime/60000)
    if( totalMin <= 15){
        pay = 500
    
    }else if(totalMin <=30){
        pay = 1000
    }else if(totalMin <=60){
        pay = 1500
    }else{
        pay = 2000
    }


    document.getElementById("time-stop").innerHTML = stopDay.toLocaleString('en-US', {hour: 'numeric', minute: 'numeric',hour12:true});
    document.getElementById("minute").innerHTML = totalMin;
    document.getElementById("payment").innerHTML = pay
}


// ----------------- Function Clear ------------------------------------
document.getElementById("btn-clear").onclick = ()=>{

    document.getElementById("btn-start").style.display = 'block'
    document.getElementById("btn-stop").style.display = 'none'
    document.getElementById("btn-clear").style.display = 'none'


    document.getElementById("time-start").innerHTML = "0:00"
    document.getElementById("time-stop").innerHTML = "0:00"
    document.getElementById("minute").innerHTML = '0'
    document.getElementById("payment").innerHTML = '0'
}
